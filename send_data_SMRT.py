from datetime import datetime
import sqlite3
import time
import requests
import json
import pytz

domain_serving_image = 'http://34.87.103.197:8080/'
domain_SMRT = 'http://34.87.103.197:9999/'

# domain_serving_image = 'http://192.168.208.1:8080/'
# domain_SMRT = 'http://192.168.208.1:9999/'

IST = pytz.timezone('Asia/Ho_Chi_Minh')
connCamera = sqlite3.connect('camera.sqlite')


def send_post_process(id_camera, weight, image_folder):
    global connCamera
    # get area camera
    cursor = connCamera.execute("SELECT area from Cameras WHERE ID = '"+id_camera+"'")
    for row in cursor:
        area = row[0]

    time_now = datetime.now(IST).strftime('%d-%m-%Y %H:%M')
    link = domain_SMRT + 'api/update_cameras'
    link_image = domain_serving_image + 'static/' + image_folder
    weight = int(float(weight) / float(area) * 100)
    if weight > 99:
        weight = 99
    r = requests.post(link,
                      json={
                          "id": id_camera,
                          "weight": weight,
                          "time": str(time_now),
                          "image": link_image,
                          "can_go": True
                      }
                      )
    print({"id": id_camera,
                          "weight": weight,
                          "time": str(time_now),
                          "image": link_image,
                          "can_go": True}
          )
    #print(r.status_code, r.reason)


conn = sqlite3.connect('AI_DATA_2.sqlite')


while True:
    cursor = conn.execute("SELECT ID, ID_CAMERA, WEIGHT, IMAGE_LINK from AIDATA")
    index = 1

    for row in cursor:
        try:
            send_post_process(str(row[1]), str(row[2]), str(row[3]))
            index += 1
            time.sleep(1)
            if index == 21:
                time.sleep(20)
                index = 0
        except:
            print('error')




