FROM python:alpine3.7
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD python ./send_data_SMRT.py
#CMD python ./api_serving_image.py
